#include <windows.h> 
// #include <winsock2.h> 
// #include <ws2tcpip.h> 

#include <BlackMagic-Interface.h> 

typedef int bool; 

#define true 1 
#define false 0 

DWORD WINAPI myServerThread (LPVOID lpParam); 
DWORD WINAPI myBackendThread (LPVOID lpParam); 

#define TALLY_PORT 50512 

BOOL isNegative (HRESULT result) { 
	BOOL negative = FALSE; 
	asm { 
		mov eax, dword ptr [result] 
		test eax, eax 
		jns sign_positive 
		
		mov dword ptr [negative], 1 
		
		sign_positive: 
	} 
	return negative; 
} 

DWORD enableTimer () { 
	return SetTimer (NULL, 0, 25, (void *) NULL); 
} 
void disableTimer (DWORD timerId) { 
	KillTimer (NULL, timerId); 
} 

bool gInTransition = false; 
long long gProgram = 0; 
long long gPreview = 2; 
bool gKeepRunning = true; 

void restart_server () { 
	gKeepRunning = false; 
	const char * procName = "BlackMagic-Tally-Server.exe"; 
	const char * procPath = "BlackMagic-Tally-Server.exe"; 
	STARTUPINFO atemStartInfo; 
	for (unsigned int i = 0; i < sizeof (STARTUPINFO); i++) 
		((char *)(&atemStartInfo)) [i] = 0; 
	atemStartInfo.cb = sizeof (STARTUPINFO); 
	PROCESS_INFORMATION atemProcInfo; 
	CreateProcess (procPath, (char *) NULL, NULL, NULL, FALSE, 0, NULL, (char *) NULL, &atemStartInfo, &atemProcInfo); 
} 

int main (int argc, char * argv []) { 
	ruvim_init ("Tally-Server-BM-Log.txt", FALSE); 
	if (!bm_init ()) { 
		print ("Error initializing BlackMagic API. \r\n"); 
	} 
	DWORD serverThreadId = 0; 
	CreateThread (NULL, // Default security attributes. 
				0, // Default stack size. 
				myServerThread, 
				(LPVOID) NULL, // No argument. 
				0, // Default creation flags. 
				&serverThreadId); 
	MSG msg; 
	DWORD timer = enableTimer (); 
	while (GetMessageA (&msg, 0, 0, 0) && gKeepRunning) { 
		switch (msg.message) { 
			case WM_CLOSE: 
				gKeepRunning = false; 
				break; 
			case WM_TIMER: 
				if (msg.wParam == timer) { 
					// print ("Timer "); 
					gInTransition = bm_is_in_transition (); 
					if ((!bm_get_program_input (&gProgram) || !bm_get_preview_input (&gPreview)) && 
					isNegative ((HRESULT) bm_restart ())) { 
						print ("Error accessing BlackMagic API. Restarting this server in 5 seconds ... \r\n"); 
						restart_server (); 
					} 
					// print ("done\r\n"); 
					// gProgram++; 
					// gPreview++; 
					// gProgram &= 0x3; 
					// gPreview &= 0x3; 
				} 
				break; 
			default: 
			; 
		} 
	} 
	print ("Cleaning up ... \r\n"); 
	disableTimer (timer); 
	bm_close (); 
	ruvim_cleanup (); 
	return 0; 
} 

DWORD WINAPI myServerThread (LPVOID lpParam) { 
	print ("Starting server ... \r\n"); 
	int iResult; 
	WSADATA wsaData; 
	SOCKET serverSocket = INVALID_SOCKET; 
	SOCKET clientSocket = INVALID_SOCKET; 
	struct addrinfo * result = NULL; 
	struct sockaddr_in server; 
	struct sockaddr_in client; 
	iResult = WSAStartup (MAKEWORD (2, 2), &wsaData); 
	if (iResult) { 
		print ("WSAStartup failed; error: "); 
		printHex (iResult); 
		print ("\r\n"); 
		return 1; 
	} 
	// ZeroMemory (&hints, sizeof (hints)); 
	// hints.ai_family = AF_INET; 
	// hints.ai_socktype = SOCK_STREAM; 
	// hints.ai_protocol = IPPROTO_TCP; 
	// hints.ai_flags = AI_PASSIVE; 
	serverSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 
	if (serverSocket == INVALID_SOCKET) { 
		DWORD error = WSAGetLastError (); 
		print ("Error creating server socket. WSA last error: "); 
		printHex (error); 
		print ("\r\n"); 
		return 2; 
	} 
	server.sin_family = AF_INET; 
	server.sin_addr.s_addr = INADDR_ANY; 
	server.sin_port = htons ( TALLY_PORT ); 
	if (bind (serverSocket, (struct sockaddr *) &server, sizeof (server)) == SOCKET_ERROR) { 
		DWORD error = WSAGetLastError (); 
		print ("Bind failed with WSA error code: "); 
		printHex (error); 
		print ("\r\n"); 
		return 3; 
	} 
	if (listen (serverSocket, SOMAXCONN)) { 
		DWORD error = WSAGetLastError (); 
		print ("Socket listen error. WSA error code: "); 
		printHex (error); 
		print ("\r\n"); 
		return 4; 
	} 
	print ("Listening for connections on port "); 
	printDec (TALLY_PORT); 
	print (" ... \r\n"); 
	int clientSize = sizeof (client); 
	DWORD lastBackendThreadId = 0; 
	while (gKeepRunning) { 
		clientSocket = accept (serverSocket, (struct sockaddr *) (&client), &clientSize); 
		if (clientSocket == INVALID_SOCKET) { 
			DWORD error = WSAGetLastError (); 
			print ("Warning:  Invalid socket received from accept (); WSA error: "); 
			printHex (error); 
			print ("\r\n"); 
			continue; 
		} 
		CreateThread (NULL, // Default security attributes. 
				0, // Default stack size. 
				myBackendThread, 
				(LPVOID) (clientSocket), // No argument. 
				0, // Default creation flags. 
				&lastBackendThreadId); 
	} 
	closesocket (serverSocket); 
	WSACleanup (); 
	return 0; 
} 

DWORD WINAPI myBackendThread (LPVOID lpParam) { 
	bool lInTransition = gInTransition; 
	long long lProgram = gProgram; 
	long long lPreview = gPreview; 
	SOCKET clientSocket = (SOCKET) lpParam; 
	unsigned char buffer [4]; 
	bool needUpdate = true; 
	ZeroMemory (buffer, 4); 
	while (gKeepRunning) { 
		if (lInTransition != gInTransition) { 
			lInTransition = gInTransition; 
			needUpdate = true; 
		} 
		if (lProgram != gProgram) { 
			lProgram = gProgram; 
			needUpdate = true; 
		} 
		if (lPreview != gPreview) { 
			lPreview = gPreview; 
			needUpdate = true; 
		} 
		if (needUpdate) { 
			// Send an update to the client. 
			buffer[0] = lInTransition ? 2 : 1; 
			buffer[1] = (unsigned char) (lProgram & 0xFF); 
			buffer[2] = (unsigned char) (lPreview & 0xFF); 
			int bSent = send (clientSocket, (char *) buffer, 4, 0); 
			if (bSent == SOCKET_ERROR) { 
				DWORD error = WSAGetLastError (); 
				print ("The send () for a status update resulted in a SOCKET_ERROR; WSA code: "); 
				printHex (error); 
				print ("\r\n"); 
				break; 
			} 
		} 
		if (!needUpdate) 
			Sleep (10); 
		needUpdate = false; 
	} 
	closesocket (clientSocket); 
	return 0; 
} 

