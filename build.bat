mkdir TemporaryBuildFiles
nasm -fobj ..\..\BlackMagic-Tools\Sources\BlackMagic-Interface.asm -o .\BlackMagic-Interface.obj
dmc ..\..\BlackMagic-Tools\Sources\thread-search -i ..\..\BlackMagic-Tools\Headers -c
dmc BlackMagic-Tally-Server BlackMagic-Interface.obj thread-search.obj ole32.lib oleaut32.lib wsock32.lib -o BlackMagic-Tally-Server.exe -i ..\..\BlackMagic-Tools\Headers
@echo off
move *.map TemporaryBuildFiles
move *.obj TemporaryBuildFiles