#include <windows.h> 

#include "BlackMagic-API.hpp" 

int main (int argc, char * argv []) { 
	OFSTRUCT ofs; 
	HANDLE hIpAddress = OpenFile ("atem-ip.txt", &ofs, 0); 
	if (!hIpAddress) { 
		print ("Error opening file to read ATEM IP address. \r\n"); 
		return 1; 
	} 
	char atem_ip [512]; 
	DWORD bRead = 0; 
	ReadFile (hIpAddress, atem_ip, 512, &bRead, NULL); 
	CloseHandle (hIpAddress); 
	if (bRead < 7) { 
		print ("Invalid ip address: "); 
		printText (atem_ip, bRead); 
		return 2; 
	} 
	if (isNegative (CoCreateInstance (NULL))) { 
		DWORD error = GetLastError (); 
		print ("Error creating COM instance. \r\n"); 
		print ("Last Error: "); 
		printHex (error); 
		print ("\r\n"); 
		return 3; 
	} 
	
	return 0; 
} 

