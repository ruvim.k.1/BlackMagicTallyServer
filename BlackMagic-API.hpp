
DWORD printText (const char * s, unsigned int l) { 
	DWORD bWritten = 0; 
	WriteFile (GetStdHandle (STD_OUTPUT_HANDLE), s, l, &bWritten, 0); 
} 
DWORD print (const char * s) { 
	return printText (s, strlen (s)); 
} 
DWORD printHex (long long iValue) { 
	char hex [17] = { '0' }; 
	char * p = &hex[15]; 
	long long v = iValue; 
	hex[16] = (char) NULL; 
	while (p >= hex) { 
		char c = v & 0x0F; 
		if (c && v) 
			*p = c >= 0x0A ? 
				c - 0x0A + 'A' 
				: c + '0'; 
		else *p = ' '; 
		p--; 
		v = v >> 4; 
	} 
	DWORD bWritten = 0; 
	WriteFile (GetStdHandle (STD_OUTPUT_HANDLE), hex, 16, &bWritten, 0); 
	return bWritten; 
} 

BOOL isNegative (HRESULT result) { 
	BOOL negative = FALSE; 
	asm { 
		mov eax, dword ptr [result] 
		test eax, eax 
		jns sign_positive 
		
		mov dword ptr [negative], 1 
		
		sign_positive: 
	} 
	return negative; 
} 

#define INPUT_BLK		0x000 
#define INPUT_COL1		0x7D1 
#define INPUT_BARS		0x3E8 
#define INPUT_MP1		0xBC2 
#define INPUT_MP2		0xBCC 

#define MOD_ALT 		0x0001 
#define MOD_CTRL		0x0002 
#define MOD_SHIFT		0x0004 
#define MOD_WIN 		0x0008 
#define MOD_NOREPEAT	0x4000 

unsigned char CBMDSwitcherDiscovery [] = { 0xE9, 0x6C, 0x72, 0xBA, 
								0xF5, 0xB8, 
								0x1B, 0x4B, 
								0xAA, 0x00, 
								0x1A, 0x2D, 0xF3, 0x99, 0x8B, 0x45 
}; 
unsigned char IBMDSwitcherDiscovery [] = { 0x87, 0xAB, 0xEF, 0x2C, 
								0xE6, 0x89, 
								0x2F, 0x44, 
								0xA4, 0xF6, 
								0x8F, 0xE6, 0x3A, 0x50, 0xE1, 0x7E 
}; 
unsigned char IBMDSwitcherMixEffectBlockIterator [] = { 0x3B, 0xDE, 0x0B, 0x93, 
								0x78, 0x4A, 
								0xD0, 0x43, 
								0x8F, 0xD3, 
								0x6E, 0x82, 0xAB, 0xA0, 0xE1, 0x17 
}; 
unsigned char IBMDSwitcherKeyIterator [] = { 0x73, 0x74, 0xC5, 0xEC, 
								0x93, 0x09, 
								0x4F, 0x44, 
								0xB3, 0xCF, 
								0xED, 0x59, 0x3C, 0xA2, 0x5B, 0x09 
}; 
unsigned char IBMDSwitcherInputIterator [] = { 0x88, 0x98, 0x5E, 0x27, 
								0x65, 0x2F, 
								0x2E, 0x4B, 
								0x94, 0x34, 
								0x19, 0x37, 0xA7, 0x2B, 0x9E, 0xC4 
}; 
unsigned char IBMDSwitcherInputAux [] = { 0xA8, 0x45, 0xC7, 0x52, 
								0xB1, 0x89, 
								0x9A, 0x44, 
								0xA1, 0x49, 
								0xC4, 0x3F, 0x51, 0x08, 0xDA, 0xE7 
}; 

public class IUnknown { 
	virtual ULONG AddRef (IUnknown * self); 
	virtual HRESULT QueryInterface (IUnknown * self, unsigned char * riid, void ** ppvObject); 
	virtual ULONG Release (IUnknown * self); 
} 

public class IBMDSwitcherDiscovery : IUnknown { 
	virtual HRESULT ConnectTo (IUnknown * self, BSTR deviceAddress, IBMDSwitcher ** switcherDevice, DWORD * failReason); 
} 






